---
title: JavaScript Sort
date: 2021-12-22 11:49:40
tags:
---


In JavaScript, we can sort data by using sort method. We can use sort for sorting

* Strings
* Numbers
* Dates

Lets looks at every topic.

## Sorting Numbers
In JavaScript, the array.sort() method sorts the array. In following ways we sort the numbers using array.sort() method.

#### Without Arguments ####
The array.sort() methods sorts the array and returns the array of sorted values. By default it sorts in ascending order and mutates the array and returns.

Try this with an example

```
let numbers = [3,2,1];

console.log(numbers.sort()) // returns [1,2,3]
```

We get an array of numbers which are in ascending order. Lets try this with anothe example

```
let numbers2 = [2,100,5]
console.log(numbers2.sort()) // returns [ 100, 2, 5 ]
```
It gives an array in ascending but it is not looking like an array of numbers which are in ascending order. What actually happening in the background is when called without arguments, the array items are converting to strings and then comparing their sequences of uni-code point values.


#### With Arguments ####

Sort method accepts arguments as well. we can use comparing functions as arguments to sort method. By using these comparing functions we sort the value as desired manner.

```
const numbers = [2, 100, 5];
numbers.sort((a, b) => {
  if (a < b) {
    return -1;
  }
  if (a > b) {
    return 1;
  }
  return 0;
}); // => [2, 5, 100]
```
Now we can see that the numbers are correctly sorted in ascending order.


## Sorting Strings ##

In JavaScript, sort method sorting the according to alphabetical order i.e it converts the given elements into strings by comparing the UTF-16 code sequences into unit values. Let's look at an example

```
let names = ['smith', 'john', 'mithchel', 'harry'];
'
names.sort();

// returns [ 'harry', 'john', 'mithchel', 'smith' ]
```

The sort() method sorts elements with the smallest unicode value at the start and largest at last. We can pass comparing functions as optional parameters to sort method to define customized sorting order.

We can sort the array of objects using their property names by passing a comparing function to the sort method.

```
const persons = [
  { id:1, name: 'mithchel'},
  { id:2, name: 'smith'},
  { id:3, name: 'john'},
  { id:4, name: 'harry'},
];

// sort by name
items.sort(function (a, b) {
  return b.name - a.name;
});
```

The above example shows how to sort an array of objects in descending order using sort method by passing comparing function as argument.


## Sorting Dates ##

By using sort method, we can sort dates as well. Let's look at an example

```
const tasks = [
    { id:1, taskName: 'Writing', date: '2019-06-28' },
    { id:3, taskName: 'Skeetching', date: '2019-06-10' },
    { id:4, taskName: 'Dancing', date: '2019-06-22' }
  ];

  const sortedTasks = tasks.sort((a,b) => {
      return a.date - b.date;
  });
```

In above example we observe that there is an array of tasks which each one has a date string with a value. If we try to use sort method using a comparing function, it does not sort the array because unicoded value of each one is same. The returned sorted array looks like the same as sorted array.

```
[
  { id: 1, taskName: 'Writing', date: '2019-06-28' },
  { id: 3, taskName: 'Skeetching', date: '2019-06-10' },
  { id: 4, taskName: 'Dancing', date: '2019-06-22' }
]
```

We have to convert the date string into date format to do sort operation.

```
const tasks = [
    { id:1, taskName: 'Writing', date: '2019-06-28' },
    { id:3, taskName: 'Skeetching', date: '2019-06-10' },
    { id:4, taskName: 'Dancing', date: '2019-06-22' }
  ];

  const sortedTasks = tasks.sort((a,b) => {
      return new Date(a.date) - new Date(b.date);
  });
```
The above code returns the sorted array in ascending format. In this example, JavaScript implicitly converts the date string and compare the unicode of each property in array and returns the sorted array.

```
[
  { id: 3, taskName: 'Skeetching', date: '2019-06-10' },
  { id: 4, taskName: 'Dancing', date: '2019-06-22' },
  { id: 1, taskName: 'Writing', date: '2019-06-28' }
]
```


## Note ##

* We can customize the order of sorting by using comparing functions.
* Sort method converts the value into string and compares uni-code point values.
* By default, sort methods sorts the array in ascending order.
