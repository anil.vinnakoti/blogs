---
title: Higher Order Function Chaining in JavaScript
date: 2021-12-22 11:57:50
tags:
---

Let us assume, we have higher order functions in our code which are performing different operations according to their usage. We have a situation like this, "Calculate the sum of odd numbers in given array".

We have to get the odd numbers first


```
let array = [1,2,3,4,5,6,7,8,9];

let oddNumbers = array.filter((number) => {
    return number%2 == 1
});
```

After finding odd number we have calculate the sum of those numbers

```
sumOfOddNumbers = oddNumbers.reduce((a,b) => {
    return a+b
})
```

We have used two variables to store two results and extra lines of codes for doing calculation. But we can do this more easier to write and more readable by using higher order function chaining.

## Higher Order Function Chaining ##
It means we can chain the higher oreder function one with one to get the job done with less code of lines and it will be easier to read.

In this example we try to solve the above calculation using higher order function chaining
```
let array = [1,2,3,4,5,6,7,8,9];

let sumOfOddNumbers = array.filter((number) => {
    return number % 2 == 1;
})
.reduce((a,b) => {
    return a+b;
});
```

In this way we can chain multiple higher order functions. But there are some key points to remember while chaining higher order functions:

* Every higher order function should return a value.
* The value returned by a higher order function can be usable by other function which is we are chaining to it.